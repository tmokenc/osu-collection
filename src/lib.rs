use serde::Deserialize;
use sqlx::sqlite::{Sqlite, SqliteConnectOptions, SqlitePool, SqlitePoolOptions};
use sqlx::FromRow;
use std::path::{Path, PathBuf};

pub type Result<T> = sqlx::Result<T>;

#[derive(Debug)]
pub struct Client {
    client: SqlitePool,
    path: PathBuf,
}

#[derive(Debug, Deserialize, FromRow)]
#[sqlx(rename_all = "PascalCase")]
pub struct OsuFile {
    filename: String,
    hash: String,
}

impl OsuFile {
    pub fn name(&self) -> &str {
        &self.filename
    }

    pub fn hash(&self) -> &str {
        &self.hash
    }

    pub fn locate(&self) -> String {
        let first = self.hash.chars().nth(0).unwrap();
        let second = self.hash.chars().nth(1).unwrap();
        format!("{}/{}{}/{}", first, first, second, self.hash)
    }
}

#[derive(Debug)]
pub struct Beatmap<'a> {
    client: &'a Client,
    inner: BeatmapInner,
}

#[derive(Debug, Deserialize)]
pub struct BeatmapMetadata {}

#[derive(Debug, Deserialize)]
struct BeatmapInner {
    date_added: String,
    online_beatmap_set_id: u64,
    metadata: BeatmapMetadata,
}

impl Client {
    pub async fn new(osu_path: impl AsRef<Path>) -> Result<Self> {
        let path = osu_path.as_ref().to_owned();
        let client_path = path.join("client.db");
        let opt = SqliteConnectOptions::new()
            .filename(client_path)
            .read_only(true);

        let client = SqlitePoolOptions::new()
            .max_connections(4)
            .connect_with(opt)
            .await?;

        Ok(Self { client, path })
    }

    pub async fn file_info(&self, hash: &str) -> Result<Option<OsuFile>> {
        let query = format!(
            "
            SELECT 
                Filename,
                Hash
            FROM BeatmapSetFileInfo
            INNER JOIN FileInfo on FileInfo.ID = BeatmapSetFileInfo.FileInfoID;
            WHERE Hash = {hash}
        "
        );

        sqlx::query_as::<Sqlite, OsuFile>(&query)
            .fetch_optional(&self.client)
            .await
    }

    pub async fn beatmaps<'a>(&'a self) -> Result<Beatmap<'a>> {
        todo!()
    }
}
